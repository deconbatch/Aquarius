/**
 * Aquarius.
 * flowing constellations with Perlin noise.
 * 
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.02.16
 */

void setup() {
  size(720, 440);
  colorMode(HSB, 360, 100, 100, 100);
  blendMode(ADD);
  noLoop();
}

void draw() {
  int   frmMax   = 24 * 8; // 24fps x 8s animation with ease scene
  int   sceneMax = 2;      // scene number
  int   starMax  = 1000;   // star number
  float baseHue  = random(360.0);
  PVector stars[] = new PVector[starMax];

  // make starfield
  for (int lNum = 0; lNum < starMax; lNum++) {
    if (lNum < starMax * 0.5) {
      float sY = map(lNum, 0, starMax * 0.5, -height * 0.35, height * 1.35);
      stars[lNum] = new PVector(random(-width * 0.35, width * 1.35), sY);
    } else {
      float sX = map(lNum, starMax * 0.5, starMax, -width * 0.35, width * 1.35);
      stars[lNum] = new PVector(sX, random(-height * 0.35, height * 1.35));
    }
  }
  
  for (int scene = 0; scene < sceneMax; scene++) {
    for (int frmCnt = 0; frmCnt < frmMax; ++frmCnt) {
      float frmRatio  = map(frmCnt, 0, frmMax, 0.0, 1.0);
      float easeRatio = easeInOutCubic(frmRatio);
      float fading    = constrain(sin(map(frmCnt + frmMax * scene, 0, frmMax * sceneMax, 0.0, PI)) * 30.0, 0.0, 1.0);

      baseHue += easeRatio * 0.5;
    
      background(baseHue % 360.0, 100.0, 60.0 * fading, 100.0);
      noStroke();
      for (int starCnt = 0; starCnt < starMax; starCnt++) {
        float stRatio = map(starCnt, 0, starMax, 0.0, 1.0);
        float stNoise = map(noise(stRatio), 0.0, 1.0, -1.0, 1.0);
        float frNoise = noise(frmRatio);
        float esNoise = noise(easeRatio);
        float coNoise = map(noise(stars[starCnt].x / width, stars[starCnt].y / height), 0.0, 1.0, -1.0, 1.0);
        float szRatio = sin(TWO_PI * (stNoise * 2.0 + frNoise * 3.0));

        stars[starCnt].x += 0.5 * (cos(TWO_PI * stRatio) * 0.2 + coNoise + stNoise - frNoise + easeRatio * easeRatio * 2.0);
        stars[starCnt].y += 0.5 * (sin(TWO_PI * stRatio) * 0.2 + coNoise + stNoise - frNoise + easeRatio * 1.2);
        float sSiz = map(szRatio, -1.0, 1.0, 2.0, 8.0);
        float sBri = map(szRatio, -1.0, 1.0, 100.0, 5.0);
        fill((baseHue + 90.0 + stRatio * 60.0) % 360.0, 60.0, sBri * fading, 100.0);
        ellipse(stars[starCnt].x, stars[starCnt].y, sSiz, sSiz);
      }

      strokeWeight(3.0);
      stroke((baseHue + 120.0) % 360.0, 40.0, 15.0 * fading, 100.0);
      noFill();
      for (int i = 0; i < starMax - 1; i++) {
        for (int j = i + 1; j < starMax; j++) {
          float d = dist(stars[i].x, stars[i].y, stars[j].x, stars[j].y);
          if (d > 20 && d < 30) {
            line(stars[i].x, stars[i].y, stars[j].x, stars[j].y);
            continue;
          }
        }
      }
      saveFrame("frames/" + String.format("%04d", scene) + String.format("%04d", frmCnt) + ".png");
    }
  }
  exit();
}

/**
 * easeInOutCubic easing function.
 * @param  t     0.0 - 1.0 : linear value.
 * @return float 0.0 - 1.0 : eased value.
 */
private float easeInOutCubic(float t) {

  t *= 2.0;
  if (t < 1.0) {
    return pow(t, 3) / 2.0;
  }
  t -= 2.0;
  return (pow(t, 3) + 2.0) / 2.0;

}
